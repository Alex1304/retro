package fr.umlv.retro.transform;

import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;

import fr.umlv.retro.core.RetroFailedException;

/**
 * Object shared between {@link RetroClassVisitor} and the features, it allows
 * features to escalate information such as flags or modified visitors to the
 * RetroClassVisitor as the instructions are being rewritten.
 */
public class RewriteNotifier {
	
	private boolean hasRewritten;
	private boolean requestedCodeRemoval;
	private FieldVisitor rewrittenFieldVisitor;
	private MethodVisitor rewrittenMethodVisitor;

	public void notifyRewrite() {
		if (hasRewritten) {
			throw new RetroFailedException("Conflict: two or more features attempted to rewrite the same element");
		}
		this.hasRewritten = true;
	}

	public void requestCodeRemoval() {
		if (hasRewritten) {
			throw new RetroFailedException("Conflict: a feature attempted to remove code while another attempted to rewrite it");
		}
		this.requestedCodeRemoval = true;
	}
	
	void resetHasRewrittenFlag() {
		this.hasRewritten = false;
	}
	
	void resetRequestedCodeRemovalFlag() {
		this.requestedCodeRemoval = false;
	}
	
	boolean hasRewritten() {
		return hasRewritten;
	}
	
	boolean hasRequestedCodeRemoval() {
		return requestedCodeRemoval;
	}

	public MethodVisitor getRewrittenMethodVisitor() {
		return rewrittenMethodVisitor;
	}

	public void setRewrittenMethodVisitor(MethodVisitor rewrittenMethodVisitor) {
		this.rewrittenMethodVisitor = rewrittenMethodVisitor;
	}

	public FieldVisitor getRewrittenFieldVisitor() {
		return rewrittenFieldVisitor;
	}

	public void setRewrittenFieldVisitor(FieldVisitor rewrittenFieldVisitor) {
		this.rewrittenFieldVisitor = rewrittenFieldVisitor;
	}
}
