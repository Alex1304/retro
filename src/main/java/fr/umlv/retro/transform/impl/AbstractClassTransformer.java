package fr.umlv.retro.transform.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;

import fr.umlv.retro.transform.ClassTransformer;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public abstract class AbstractClassTransformer implements ClassTransformer {

	private final LinkedHashSet<Path> directoriesToCreate = new LinkedHashSet<>();
	private final LinkedHashSet<FileToSave> filesToSave = new LinkedHashSet<>();

	@Override
	public void flush() throws IOException {
		for (var directoryToCreate : directoriesToCreate) {
			Files.createDirectories(directoryToCreate);
		}
		for (var fileToSave : filesToSave) {
			Files.write(fileToSave.path, fileToSave.data);
		}
	}
	
	void saveLater(Path path, byte[] data) {
		filesToSave.add(new FileToSave(path, data));
	}
	
	void saveSynthesizedClassesLater(Path dir, SynthesizedClassRegistry synthClassRegistry) throws IOException {
		
		for (var synthesizedClass : synthClassRegistry) {
			var pathToResolve = Path.of(".", (synthesizedClass.getInternalName() + ".class").split("/"));
			var synthClassPath = dir.toAbsolutePath().resolve(pathToResolve);
			directoriesToCreate.add(synthClassPath.getParent());
			saveLater(synthClassPath, synthesizedClass.getBytes());
		}
	}
	
	private static class FileToSave {
		private final Path path;
		private final byte[] data;
		
		public FileToSave(Path path, byte[] data) {
			this.path = path;
			this.data = data;
		}
	}
}
