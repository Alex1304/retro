package fr.umlv.retro.transform.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public class JarClassTransformer extends AbstractClassTransformer {

	@Override
	public boolean supports(Path path) {
		return Files.isRegularFile(path) && path.toString().endsWith(".jar");
	}

	@Override
	public void accept(Path path, Consumer<byte[]> consumer) throws IOException {
		try (var in = new ZipInputStream(Files.newInputStream(path))) {
			for (var zipEntry = in.getNextEntry() ; zipEntry != null ; zipEntry = in.getNextEntry()) {
				if (zipEntry.getName().endsWith(".class")) {
					var zipEntryBytes = in.readAllBytes();
					consumer.accept(zipEntryBytes);
				}
				in.closeEntry();
			}
		}
	}

	@Override
	public void transform(Path path, SynthesizedClassRegistry synthClassRegistry, UnaryOperator<byte[]> transformer) throws IOException {
		try (var in = new ZipInputStream(Files.newInputStream(path))) {
			var zipEntries = new LinkedHashMap<ZipEntry, byte[]>();
			for (var zipEntry = in.getNextEntry() ; zipEntry != null ; zipEntry = in.getNextEntry()) {
				var zipEntryBytes = in.readAllBytes();
				in.closeEntry();
				zipEntries.put(zipEntry, zipEntryBytes);
			}
			var byteArrayOutputStream = new ByteArrayOutputStream();
			try (var out = new ZipOutputStream(byteArrayOutputStream)) {
				for (var entry : zipEntries.entrySet()) {
					var zipEntry = entry.getKey();
					var zipEntryBytes = entry.getValue();
					if (zipEntry.getName().endsWith(".class")) {
						zipEntryBytes = transformer.apply(zipEntryBytes);
					}
					out.putNextEntry(new ZipEntry(zipEntry.getName()));
					out.write(zipEntryBytes);
					out.closeEntry();
				}
				for (var synthesizedClass : synthClassRegistry) {
					out.putNextEntry(new ZipEntry(synthesizedClass.getInternalName() + ".class"));
					out.write(synthesizedClass.getBytes());
					out.closeEntry();
				}
				out.flush();
			}
			saveLater(path, byteArrayOutputStream.toByteArray());
		}
	}
}
