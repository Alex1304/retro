package fr.umlv.retro.transform.impl;

import java.io.IOException;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public class SingleClassTransformer extends AbstractClassTransformer {

	@Override
	public boolean supports(Path path) {
		return ClassFiles.isClassFile(path);
	}

	@Override
	public void accept(Path path, Consumer<byte[]> consumer) throws IOException {
		ClassFiles.accept(path, consumer);
	}

	@Override
	public void transform(Path path, SynthesizedClassRegistry synthClassRegistry, UnaryOperator<byte[]> transformer) throws IOException {
		saveLater(path, ClassFiles.transform(path, transformer));
		saveSynthesizedClassesLater(path.toAbsolutePath().getParent(), synthClassRegistry);
	}
}
