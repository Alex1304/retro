package fr.umlv.retro.transform.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

class ClassFiles {
	
	private ClassFiles() {
	}

	/**
	 * Reads a class file without transforming it.
	 * 
	 * @param file     the class file to read
	 * @param consumer the action to execute on each class
	 * @throws IOException              if any I/O exception occurs during the
	 *                                  process
	 * @throws IllegalArgumentException if it isn't a class file
	 */
	static void accept(Path file, Consumer<byte[]> consumer) throws IOException {
		if (!isClassFile(file)) {
			throw new IllegalArgumentException("Attempt to accept a non-class file");
		}
		consumer.accept(Files.readAllBytes(file));
	}

	/**
	 * Transforms a class file.
	 * 
	 * @param file        the file to transform
	 * @param transformer the operator that tells how to transform the class
	 * @throws IOException              if any I/O exception occurs during read
	 * @throws IllegalArgumentException if it isn't a class file
	 */
	static byte[] transform(Path file, UnaryOperator<byte[]> transformer) throws IOException {
		if (!isClassFile(file)) {
			throw new IllegalArgumentException("Attempt to transform a non-class file");
		}
		return transformer.apply(Files.readAllBytes(file));
	}
	
	/**
	 * Tests if the path refers to a class file. It only checks for the file
	 * extension and does not check if the content is a valid class file.
	 * 
	 * @param path the file to test
	 * @return true if it's a .class, false otherwise
	 */
	static boolean isClassFile(Path path) {
		return Files.isRegularFile(path) && path.toString().endsWith(".class");
	}
}
