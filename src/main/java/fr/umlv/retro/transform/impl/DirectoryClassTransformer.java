package fr.umlv.retro.transform.impl;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public class DirectoryClassTransformer extends AbstractClassTransformer {

	@Override
	public boolean supports(Path path) {
		return Files.isDirectory(path);
	}

	@Override
	public void accept(Path path, Consumer<byte[]> consumer) throws IOException {
		forEachClassFile(path, file -> {
			try {
				ClassFiles.accept(file, consumer);
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		});
	}

	@Override
	public void transform(Path path, SynthesizedClassRegistry synthClassRegistry, UnaryOperator<byte[]> transformer) throws IOException {
		forEachClassFile(path, file -> {
			try {
				saveLater(file, ClassFiles.transform(file, transformer));
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		});
		saveSynthesizedClassesLater(path, synthClassRegistry);
	}
	
	private void forEachClassFile(Path dir, Consumer<Path> actionOnEachClass) throws IOException {
		try (var dirList = Files.list(dir)) {
			dirList.filter(ClassFiles::isClassFile).forEach(actionOnEachClass);
		} catch (UncheckedIOException e) {
			throw e.getCause();
		}
	}
}
