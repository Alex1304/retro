package fr.umlv.retro.transform;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static java.util.stream.Collectors.toUnmodifiableSet;
import static org.objectweb.asm.Opcodes.ASM7;
import static org.objectweb.asm.Opcodes.V10;
import static org.objectweb.asm.Opcodes.V11;
import static org.objectweb.asm.Opcodes.V12;
import static org.objectweb.asm.Opcodes.V13;
import static org.objectweb.asm.Opcodes.V1_5;
import static org.objectweb.asm.Opcodes.V1_6;
import static org.objectweb.asm.Opcodes.V1_7;
import static org.objectweb.asm.Opcodes.V1_8;
import static org.objectweb.asm.Opcodes.V9;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import fr.umlv.retro.feature.Feature;
import fr.umlv.retro.feature.FeatureFactory;
import fr.umlv.retro.util.MethodInfo;
import fr.umlv.retro.util.synthesize.SynthesizeUtils;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

/**
 * Visits a class and downgrade the bytecode of the given features to a lower
 * Java version.
 */
public class RetroClassVisitor extends ClassVisitor {
	
	private final int targetVersion;
	private final Map<Feature, ClassVisitor> features;
	private final RewriteNotifier rewriteNotifier;
	
	private String className;
	

	/**
	 * Creates a new {@link RetroClassVisitor}
	 * 
	 * @param classWriter the classWriter used to write bytecode. May be null to not write anything.
	 * @param featureFactories a set of factories used to instantiate the features to downgrade
	 * @param targetVersion the target Java version to downgrade to. Pass 0 to keep the original version.
	 * @param force whether to force downgrading of unsupported features
	 * @param actionOnRecognizedFeature tells which action to perform when a feature is recognized
	 */
	public RetroClassVisitor(ClassWriter classWriter, Set<? extends FeatureFactory> featureFactories,
			SynthesizedClassRegistry synthClassRegistry, Map<String, Set<Feature>> unsupportedFeatures,
			int targetVersion, Consumer<? super Feature> actionOnRecognizedFeature) {
		super(ASM7, classWriter);
		requireNonNull(featureFactories);
		requireNonNull(actionOnRecognizedFeature);
		if (!Set.of(0, V1_5, V1_6, V1_7, V1_8, V9, V10, V11, V12, V13).contains(targetVersion)) {
			throw new IllegalArgumentException("Unsupported target version");
		}
		this.rewriteNotifier = new RewriteNotifier();
		this.targetVersion = targetVersion;
		this.features = featureFactories.stream()
				.map(factory -> factory.create(classWriter, targetVersion, synthClassRegistry, rewriteNotifier, actionOnRecognizedFeature, unsupportedFeatures))
				.collect(toUnmodifiableMap(Function.identity(), Feature::getClassVisitor));
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		this.className = name;
		features.keySet().forEach(feature -> feature.setClassName(name));
		var adaptedInterfaces = Arrays.stream(interfaces)
				.map(i -> SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, i))
				.toArray(String[]::new);
		delegateVisitToEachFeature(features.values(), cv, cv -> cv.visit(targetVersion == 0 ? version : targetVersion, access, name, signature, superName, adaptedInterfaces));
	}
	
	@Override
	public void visitSource(String source, String debug) {
		features.keySet().forEach(feature -> feature.setSource(source));
		delegateVisitToEachFeature(features.values(), cv, cv -> cv.visitSource(source, debug));
	}
	
	@Override
	public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
			String[] exceptions) {
		var adaptedDescriptor = SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, descriptor);
		var methodVisitors = collectSubvisitors(cv -> cv.visitMethod(access, name, adaptedDescriptor, signature, exceptions));
		if (rewriteNotifier.hasRequestedCodeRemoval()) {
			rewriteNotifier.resetRequestedCodeRemovalFlag();
			return null;
		}
		var methodInfo = new MethodInfo(className, name, adaptedDescriptor);
		var methodWriter = getModifiedWriterOrDefault(
				rewriteNotifier::getRewrittenMethodVisitor,
				rewriteNotifier::setRewrittenMethodVisitor,
				() -> super.visitMethod(access, name, adaptedDescriptor, signature, exceptions));
		features.keySet().forEach(feature -> feature.setMethodContext(methodInfo, methodWriter));
		return new MethodVisitor(ASM7, methodWriter) {
			@Override
			public void visitLineNumber(int line, Label start) {
				features.keySet().forEach(feature -> feature.setLineNumber(line));
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitLineNumber(line, start));
			}
			
			@Override
			public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitTryCatchBlock(start, end, handler, type));
			}
			
			@Override
			public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
				var adaptedDescriptor = SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, descriptor);
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitFieldInsn(opcode, owner, name, descriptor),
						mv -> mv.visitFieldInsn(opcode, owner, name, adaptedDescriptor));
			};

			@Override
			public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
				var adaptedDescriptor = SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, descriptor);
				var adaptedOwner = SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, owner);
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitMethodInsn(opcode, owner, name, descriptor, isInterface),
						mv -> mv.visitMethodInsn(opcode, adaptedOwner, name, adaptedDescriptor, isInterface));
			}

			@Override
			public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle,
					Object... bootstrapMethodArguments) {
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments));
			}
			
			@Override
			public void visitLabel(Label label) {
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitLabel(label));
			}

			@Override
			public void visitVarInsn(int opcode, int var) {
				delegateVisitToEachFeature(methodVisitors, methodWriter,
						mv -> mv.visitVarInsn(opcode, var));
			}
			
			@Override
			public void visitTypeInsn(int opcode, String type) {
				super.visitTypeInsn(opcode, SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, type));
			}
			
			@Override
			public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end, int index) {
				super.visitLocalVariable(name, SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, descriptor), signature, start, end, index);
			}
			
			@Override
			public void visitFrame(int type, int numLocal, Object[] local, int numStack, Object[] stack) {
				super.visitFrame(
						type,
						numLocal,
						Arrays.stream(local).map(this::adaptFrameElement).toArray(),
						numStack,
						Arrays.stream(stack).map(this::adaptFrameElement).toArray());
			}
			
			private Object adaptFrameElement(Object o) {
				if (o instanceof String) {
					return SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, (String) o);
				}
				return o;
			}
			
			@Override
			public void visitEnd() {
				delegateVisitToEachFeature(methodVisitors, methodWriter, MethodVisitor::visitEnd);
				features.keySet().forEach(feature -> feature.setMethodContext(null, null));
				super.visitEnd();
			}
		};
	}

	@Override
	public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
		var adaptedDescriptor = SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, descriptor);
		collectSubvisitors(cv -> cv.visitField(access, name, adaptedDescriptor, signature, value));
		if (rewriteNotifier.hasRequestedCodeRemoval()) {
			rewriteNotifier.resetRequestedCodeRemovalFlag();
			return null;
		}
		return getModifiedWriterOrDefault(
				rewriteNotifier::getRewrittenFieldVisitor,
				rewriteNotifier::setRewrittenFieldVisitor,
				() -> super.visitField(access, name, adaptedDescriptor, signature, value));
	}
	
	@Override
	public void visitNestHost(String nestHost) {
		delegateVisitToEachFeature(features.values(), cv, cv -> cv.visitNestHost(nestHost));
	}
	
	@Override
	public void visitNestMember(String nestMember) {
		delegateVisitToEachFeature(features.values(), cv, cv -> cv.visitNestMember(nestMember));
	}
	
	@Override
	public void visitEnd() {
		delegateVisitToEachFeature(features.values(), cv, ClassVisitor::visitEnd);
	}

	// Refactored private methods
	
	private <V> void delegateVisitToEachFeature(Collection<? extends V> visitors, V defaultVisitor, Consumer<? super V> visitAction, Consumer<? super V> defaultVisitAction) {
		visitors.forEach(visitAction);
		if (!rewriteNotifier.hasRewritten()) {
			if (defaultVisitor != null) {
				defaultVisitAction.accept(defaultVisitor);
			}
		}
		rewriteNotifier.resetHasRewrittenFlag();
	}
	
	private <V> void delegateVisitToEachFeature(Collection<? extends V> visitors, V defaultVisitor, Consumer<? super V> visitAction) {
		visitors.forEach(visitAction);
		if (!rewriteNotifier.hasRewritten()) {
			if (defaultVisitor != null) {
				visitAction.accept(defaultVisitor);
			}
		}
		rewriteNotifier.resetHasRewrittenFlag();
	}
	
	private <S> Set<S> collectSubvisitors(Function<ClassVisitor, S> subvisitorGetter) {
		return features.values().stream()
				.flatMap(cv -> Optional.ofNullable(subvisitorGetter.apply(cv)).stream())
				.collect(toUnmodifiableSet());
	}
	
	private <W> W getModifiedWriterOrDefault(Supplier<W> modifiedWriter, Consumer<W> modifiedWriterSetter, Supplier<W> defaultWriter) {
		var writer = modifiedWriter.get();
		if (writer != null) {
			modifiedWriterSetter.accept(null);
			return writer;
		}
		return defaultWriter.get();
	}
}
