package fr.umlv.retro.transform;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

import fr.umlv.retro.transform.impl.DirectoryClassTransformer;
import fr.umlv.retro.transform.impl.JarClassTransformer;
import fr.umlv.retro.transform.impl.SingleClassTransformer;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public interface ClassTransformer {
	
	/**
	 * Checks if the given path is supported by this transformer.
	 * 
	 * @param path the path to test
	 * @return true if supported, else false
	 */
	boolean supports(Path path);
	
	/**
	 * Reads the classes contained in the given path without actually performing any
	 * transformation.
	 * 
	 * @param path        the path containing the classes to accept
	 * @param transformer the action to execute according to the bytes of a class
	 *                    contained in the path
	 * @throws IOException if an I/O error occurs during read
	 */
	void accept(Path path, Consumer<byte[]> consumer) throws IOException;

	/**
	 * Reads the classes contained in the given path and applies the given
	 * transformation to them. Transformed classes aren't written on disk until
	 * {@link #flush()} is called.
	 * 
	 * @param path               the path containing the classes to transform
	 * @param synthClassRegistry a registry containing new classes synthesized
	 *                           during the transformation
	 * @param transformer        the operator that transforms the bytes of a class
	 *                           contained in the path
	 * @throws IOException if an I/O error occurs during read
	 */
	void transform(Path path, SynthesizedClassRegistry synthClassRegistry, UnaryOperator<byte[]> transformer) throws IOException;
	
	/**
	 * Writes all transformed classes on disk, if any.
	 * 
	 * @throws IOException if an I/O error occurs during write
	 */
	void flush() throws IOException;
	
	/**
	 * Gets all instances of class transformers useful for the application.
	 * 
	 * @return a Set of ClassTransformer
	 */
	static Set<ClassTransformer> getAllClassTransformers() {
		return Set.of(new DirectoryClassTransformer(), new JarClassTransformer(), new SingleClassTransformer());
	}
}
