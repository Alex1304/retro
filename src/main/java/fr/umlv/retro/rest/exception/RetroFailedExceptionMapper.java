package fr.umlv.retro.rest.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import fr.umlv.retro.core.RetroFailedException;
import fr.umlv.retro.rest.json.error.SimpleError;
import fr.umlv.retro.rest.json.error.ForceRequiredError;

@Provider
public class RetroFailedExceptionMapper implements ExceptionMapper<RetroFailedException> {
	
	@Override
	public Response toResponse(RetroFailedException exception) {
		return exception.getUnsupportedFeatures()
				.map(ForceRequiredError::from)
				.map(forceRequired -> Response.status(403).entity(forceRequired).type(MediaType.APPLICATION_JSON).build())
				.orElse(SimpleError.toResponse(400, exception.getMessage()));
	}

}
