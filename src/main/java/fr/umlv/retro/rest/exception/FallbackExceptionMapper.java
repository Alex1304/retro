package fr.umlv.retro.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import fr.umlv.retro.rest.json.error.SimpleError;

@Provider
public class FallbackExceptionMapper implements ExceptionMapper<Throwable> {

	private static final Logger LOGGER = Logger.getLogger(FallbackExceptionMapper.class);
	
	@Override
	public Response toResponse(Throwable exception) {
		LOGGER.error("A request failed because of an unhandled exception", exception);
		return SimpleError.toResponse(500, "Internal Server Error");
	}

}
