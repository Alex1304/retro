package fr.umlv.retro.rest.exception;

import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.jboss.logging.Logger;

import fr.umlv.retro.rest.json.error.SimpleError;

public class IOExceptionMapper implements ExceptionMapper<IOException> {

	private static final Logger LOGGER = Logger.getLogger(IOExceptionMapper.class);
	
	@Override
	public Response toResponse(IOException exception) {
		var message = "An I/O error occured";
		LOGGER.error(message, exception);
		return SimpleError.toResponse(500, message);
	}

}
