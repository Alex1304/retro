package fr.umlv.retro.rest.exception;

import static java.util.stream.Collectors.toUnmodifiableList;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import fr.umlv.retro.rest.json.error.ValidationError;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
	
	@Override
	public Response toResponse(ConstraintViolationException exception) {
		var error = new ValidationError("Invalid request parameters", exception.getConstraintViolations().stream()
				.map(ConstraintViolation::getMessage)
				.collect(toUnmodifiableList()));
		return Response.status(400).entity(error).type(MediaType.APPLICATION_JSON).build();
	}

}
