package fr.umlv.retro.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import fr.umlv.retro.core.InvalidParamsException;
import fr.umlv.retro.rest.json.error.SimpleError;

@Provider
public class InvalidParamsExceptionMapper implements ExceptionMapper<InvalidParamsException> {
	
	@Override
	public Response toResponse(InvalidParamsException exception) {
		return SimpleError.toResponse(400, exception.getMessage());
	}

}
