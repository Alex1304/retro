package fr.umlv.retro.rest.resource;

import java.io.IOException;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.umlv.retro.core.RetroService;
import fr.umlv.retro.rest.json.requestparams.RetroParams;

@Path("/retro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RetroRessource {
	
	@POST
	@Path("/info")
	public Response info(@Valid RetroParams rp) throws IOException {
		var infos = RetroService.runInfoFromJson(rp);
		return Response.ok(infos).build();
	}
	
	@POST
	@Path("/execute")
	public Response execute(@Valid RetroParams rp) throws IOException {
		RetroService.runTargetFromJson(rp);
		return Response.noContent().build();
	} 
}
