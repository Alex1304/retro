package fr.umlv.retro.rest.resource;

import static java.util.stream.Collectors.toUnmodifiableList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.umlv.retro.rest.json.requestparams.TreeParams;
import fr.umlv.retro.rest.json.response.TreeResponse;
import fr.umlv.retro.rest.json.response.TreeResponse.FileEntry;

@Path("/tree")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TreeResource {
	
	@POST
	public Response tree(@Valid TreeParams treeParams) throws IOException, NotADirectoryException {
		var treeResponse = listFilesAtPath(treeParams.directory);
		return Response.ok(treeResponse).build();
	}
	
	private static TreeResponse listFilesAtPath(String pathStr) throws IOException, NotADirectoryException {
		var path = Paths.get(pathStr).toAbsolutePath().normalize();
		if (!Files.isDirectory(path)) {
			throw new NotADirectoryException();
		}
		try (var stream = Files.list(path)) {
			return new TreeResponse(path.toString(), stream.map(FileEntry::fromPath).collect(toUnmodifiableList()));
		}
	}
	
	private static class NotADirectoryException extends Exception {
		private static final long serialVersionUID = 5551635977402772229L;
	}
}
