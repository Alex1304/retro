package fr.umlv.retro.rest.json.requestparams;

import java.util.List;

import javax.validation.constraints.NotEmpty;

public class RetroParams {
	
	public List<String> features = List.of();
	
	@NotEmpty(message = "path: you must provide at least 1 path")
	public List<String> paths;
	
	public String target;
	public boolean force;

	public RetroParams() {
	}

	public RetroParams(List<String> features, List<String> paths, String target, boolean force) {
		this.features = features;
		this.paths = paths;
		this.target = target;
		this.force = force;
	}

}
