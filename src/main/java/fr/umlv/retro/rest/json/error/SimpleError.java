package fr.umlv.retro.rest.json.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class SimpleError {
	
	public String message;
	
	private SimpleError(String message) {
		this.message = message;
	}

	public SimpleError() {
	}
	
	public static Response toResponse(int status, String message) {
		return Response.status(status).type(MediaType.APPLICATION_JSON).entity(new SimpleError(message)).build();
	}
}