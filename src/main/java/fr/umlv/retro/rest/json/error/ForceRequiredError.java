package fr.umlv.retro.rest.json.error;

import static java.util.stream.Collectors.toUnmodifiableList;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import fr.umlv.retro.feature.Feature;

public class ForceRequiredError {
	
	public List<UnsupportedFeature> unsupportedFeatures;
	public List<AboutFeature> aboutFeatures;
	
	public ForceRequiredError() {
	}
	
	private ForceRequiredError(List<UnsupportedFeature> unsupportedFeatures, List<AboutFeature> aboutFeatures) {
		this.unsupportedFeatures = unsupportedFeatures;
		this.aboutFeatures = aboutFeatures;
	}

	public static ForceRequiredError from(Map<String, Set<Feature>> unsupportedFeatures) {
		return new ForceRequiredError(
				unsupportedFeatures.entrySet().stream()
						.map(entry -> new UnsupportedFeature(entry.getKey(), entry.getValue().stream().map(Feature::getName).collect(toUnmodifiableList())))
						.collect(toUnmodifiableList()),
				unsupportedFeatures.values().stream()
						.flatMap(Set::stream)
						.map(feature -> new AboutFeature(feature.getName(), feature.getUnsupportedErrorMessage()))
						.distinct()
						.collect(toUnmodifiableList()));
	}
	
	public static class UnsupportedFeature {
		public String classFile;
		public List<String> features;
		
		public UnsupportedFeature() {
		}
		
		private UnsupportedFeature(String classFile, List<String> features) {
			this.classFile = classFile;
			this.features = features;
		}
	}
	
	public static class AboutFeature {
		public String name;
		public String explanation;
		
		private AboutFeature(String name, String explanation) {
			this.name = name;
			this.explanation = explanation;
		}
		
		public AboutFeature() {
		}
		
		@Override
		public boolean equals(Object obj) {
			return obj instanceof AboutFeature && Objects.equals(((AboutFeature) obj).name, name); // Objects.equals handles null cases
		}
		
		@Override
		public int hashCode() {
			return name.hashCode(); 
		}
	}
}
