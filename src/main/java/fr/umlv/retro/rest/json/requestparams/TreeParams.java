package fr.umlv.retro.rest.json.requestparams;

import javax.validation.constraints.*;

public class TreeParams {
	
	@NotNull(message = "directory: field is required")
	public String directory;
}
