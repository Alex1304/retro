package fr.umlv.retro.rest.json.response;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class TreeResponse {
	
	public String directory;
	public String separator = FileSystems.getDefault().getSeparator();
	public List<FileEntry> entries;
	
	public TreeResponse(String directory, List<FileEntry> entries) {
		this.directory = directory;
		this.entries = entries;
	}
	
	public TreeResponse() {
	}
	
	public static class FileEntry {
		
		public String path;
		public boolean isDirectory;
		
		private FileEntry(String path, boolean isDirectory) {
			this.path = path;
			this.isDirectory = isDirectory;
		}
		
		public static FileEntry fromPath(Path path) {
			return new FileEntry(path.toString(), Files.isDirectory(path));
		}
		
		public FileEntry() {
		}
	}

}
