package fr.umlv.retro.rest.json.error;

import java.util.List;

public class ValidationError {
	
	public String message;
	public List<String> violations;
	
	public ValidationError(String message, List<String> violations) {
		this.message = message;
		this.violations = violations;
	}
	
	public ValidationError() {
	}
}
