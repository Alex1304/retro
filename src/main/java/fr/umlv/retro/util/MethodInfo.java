package fr.umlv.retro.util;

/**
 * Holds information on a method that is being visited. Allows to avoid passing
 * too many arguments to the features when passing the method visiting context.
 */
public class MethodInfo {

	private final String owner;
	private final String name;
	private final String descriptor;
	
	public MethodInfo(String className, String name, String descriptor) {
		this.owner = className;
		this.name = name;
		this.descriptor = descriptor;
	}
	
	@Override
	public String toString() {
		return owner + "." + name + descriptor;
	}
}
