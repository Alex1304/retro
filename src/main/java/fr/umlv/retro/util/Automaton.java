package fr.umlv.retro.util;

import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * An automaton is a stateful consumer of values. Values that are consumed will
 * modify the internal state of the automaton until it eventually completes. It
 * is possible to check anytime the progress of the automaton and if it has
 * completed.
 * 
 * @param <E> The type of consumed values
 */
public final class Automaton<E> {
	
	// Immutable, lists all states in order and is used for initialization
	private final List<State<E>> initialStates;
	
	// Mutable, states are popped as they are being accepted
	private ArrayDeque<State<E>> remainingStates;
	
	private Automaton(List<State<E>> initialStates) {
		this.initialStates = initialStates;
		this.remainingStates = new ArrayDeque<>(initialStates);
	}
	
	/**
	 * Resets the state of the automaton to as it was on instanciation.
	 */
	public void reset() {
		this.remainingStates = new ArrayDeque<>(initialStates);
	}
	
	/**
	 * Consumes a value. The consumption may modify this automaton's state.
	 * 
	 * @param value the value to consume
	 */
	public void accept(E value) {
		requireNonNull(value);
		if (remainingStates.isEmpty()) {
			throw new IllegalStateException("Automaton is already complete, no more values are being accepted");
		}
		var currentState = remainingStates.peekFirst();
		if (currentState.conditionToMoveToNextState.test(value)) {
			remainingStates.removeFirst();
		} else {
			reset();
		}
	}
	
	/**
	 * Checks if the automaton has completed.
	 * 
	 * @return a boolean
	 */
	public boolean isComplete() {
		return remainingStates.isEmpty();
	}
	
	/**
	 * Gets the current state index, in other words, the number of times
	 * {@link #accept(Object)} modified this automaton's state since last reset.
	 * This number can range from 0 (initial state) to the number of times
	 * {@link Builder#thenExpect(Predicate, RejectionPolicy)} was called on the
	 * builder (which means the automate is complete).
	 * 
	 * @return an int
	 */
	public int getCurrentStateIndex() {
		return initialStates.size() - remainingStates.size();
	}
	
	/**
	 * Creates a new automaton builder.
	 * 
	 * @param <T>    the generic type of the builder to create
	 * @return a new builder
	 */
	public static <T> Builder<T> builder() {
		return new Builder<>();
	}
	
	/**
	 * Builder class for {@link Automaton}
	 * 
	 * @param <E> the generic type of the Automaton being built
	 */
	public static class Builder<E> {
		private final ArrayList<State<E>> definedStates = new ArrayList<>();
		
		/**
		 * Adds a new state to the automaton.
		 * 
		 * @param conditionToMoveToNextState the condition that the consumed value must
		 *                                   fulfill in order to move to next state
		 * 
		 * @return this builder
		 */
		public Builder<E> thenExpect(Predicate<? super E> conditionToMoveToNextState) {
			requireNonNull(conditionToMoveToNextState);
			definedStates.add(new State<>(conditionToMoveToNextState));
			return this;
		}
		
		/**
		 * Instantiates the automaton with the defined states.
		 * 
		 * @return a new Automaton
		 */
		public Automaton<E> build() {
			return new Automaton<>(unmodifiableList(definedStates));
		}
	}

	private static class State<E> {
		private final Predicate<? super E> conditionToMoveToNextState;
		
		public State(Predicate<? super E> conditionToMoveToNextState) {
			this.conditionToMoveToNextState = conditionToMoveToNextState;
		}
	}
}
