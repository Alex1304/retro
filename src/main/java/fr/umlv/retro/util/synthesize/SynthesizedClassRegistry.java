package fr.umlv.retro.util.synthesize;

import static java.util.Objects.requireNonNull;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class SynthesizedClassRegistry implements Iterable<SynthesizedClass> {
	
	private final LinkedHashSet<SynthesizedClass> registeredSynthesizedClasses = new LinkedHashSet<>();
	
	public void add(SynthesizedClass synthesizedClass) {
		requireNonNull(synthesizedClass);
		registeredSynthesizedClasses.add(synthesizedClass);
	}

	@Override
	public Iterator<SynthesizedClass> iterator() {
		return registeredSynthesizedClasses.iterator();
	}
	
}
