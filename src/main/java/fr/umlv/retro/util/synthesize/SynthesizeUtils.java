package fr.umlv.retro.util.synthesize;

import static java.util.Objects.requireNonNull;
import static org.objectweb.asm.Opcodes.ACC_SYNTHETIC;

import java.util.function.Consumer;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;

public class SynthesizeUtils {
	
	private SynthesizeUtils() {
	}
	
	/**
	 * Synthesizes a new method.
	 * 
	 * @param cw        the writer of the class owning the method
	 * @param access    access flags of the method
	 * @param m         the method's name and descriptor
	 * @param generator the consumer that generates the code of the method
	 */
	public static void synthesizeMethod(ClassWriter cw, int access, Method m, Consumer<GeneratorAdapter> generator)  {
		requireNonNull(cw, "cw is null");
		requireNonNull(m, "m is null");
		requireNonNull(generator, "generator is null");
		var mv = cw.visitMethod(access | ACC_SYNTHETIC, m.getName(), m.getDescriptor(), null, null);
		var g = new GeneratorAdapter(access, m, mv);
		generator.accept(g);
		g.endMethod();
	}

	/**
	 * Checks whether the retro program needs to synthesize the type corresponding
	 * to a functional interface.
	 * 
	 * @param className the name to test
	 * @return true if synthesis is needed, else false
	 */
	public static boolean needsSynthesizedFunctionalInterface(String className) {
		return className.startsWith("java/util/function") || className.equals(Type.getInternalName(AutoCloseable.class));
	}

	/**
	 * Adapts references of jdk 8+ functional interfaces to their synthesized
	 * version. Synthesized functional interfaces have their package renamed.
	 * 
	 * @param targetVersion    the target version. If below {@link Opcodes#V1_8},
	 *                         the argument is returned as is
	 * @param nameOrDescriptor the reference to test
	 * @return the adapted name
	 */
	public static String adaptFunctionalInterfaceName(int targetVersion, String nameOrDescriptor) {
		if (targetVersion < Opcodes.V1_8) {
			return nameOrDescriptor
					.replace("java/util/function", "retro$/util/function")
					.replace("java/lang/AutoCloseable", "retro$java/lang/AutoCloseable");
		}
		return nameOrDescriptor;
	}

	/**
	 * Checks if the given type is primitive.
	 * 
	 * @param t the type to check
	 * @return true if primitive, else false
	 */
	public static boolean isPrimitive(Type t) {
		return t.getSort() >= Type.BOOLEAN && t.getSort() <= Type.DOUBLE;
	}
}
