package fr.umlv.retro.util.synthesize;

import static java.util.Objects.requireNonNull;

import org.objectweb.asm.Type;

/**
 * Respresents the method containing the code of a lambda expression.
 */
public class CallingMethod {
	
	private final Type owner;
	private final String methodName;
	private final Type methodType;
	private final String source;
	
	private CallingMethod(Type owner, String methodName, Type methodType, String source) {
		this.owner = owner;
		this.methodName = methodName;
		this.methodType = methodType;
		this.source = source;
	}

	/**
	 * Gets the type of the method's owner
	 * 
	 * @return a Type
	 */
	public Type getOwner() {
		return owner;
	}

	/**
	 * Gets the name of the method
	 * 
	 * @return a String
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Gets the type of the method
	 * 
	 * @return a Type
	 */
	public Type getMethodType() {
		return methodType;
	}
	
	@Override
	public String toString() {
		return source;
	}
	
	/**
	 * Whether this calling method is a reference to an existing method.
	 * 
	 * @return true if it's a method reference, false if it's a regular lambda expression
	 */
	public boolean isMethodReference() {
		return !methodName.startsWith("lambda$");
	}
	
	/**
	 * Creates a copy of this {@link CallingMethod} object but with a new method
	 * name and owner.
	 * 
	 * @param newOwner the new owner
	 * @param newMethodName the new method name
	 * @return a new {@link CallingMethod} instance identical to this but with the
	 *         method name changed
	 */
	public CallingMethod wrapped(Type newOwner, String newMethodName) {
		requireNonNull(newOwner);
		requireNonNull(newMethodName);
		return new CallingMethod(newOwner, newMethodName, methodType, source);
	}
	
	/**
	 * Whether this calling method is virtual.
	 * 
	 * @param captured the number of captured values
	 * @param interfaceMethodArgs the number of arguments on the functional interface abstract method
	 * @return true if this calling method is virtual, false if it's static
	 */
	public boolean isVirtualMethod(int captured, int interfaceMethodArgs) {
		return methodType.getArgumentTypes().length < captured + interfaceMethodArgs;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof CallingMethod && ((CallingMethod) obj).source.equals(source);
	}
	
	@Override
	public int hashCode() {
		return source.hashCode();
	}
	
	/**
	 * Creates a new {@link CallingMethod} instance by parsing the descriptor from
	 * the given string.
	 * 
	 * @param str the string to parse
	 * @return a new {@link CallingMethod} instance
	 */
	public static CallingMethod parse(String str) {
		var tokDesc = str.split("\\(");
		var tokName = tokDesc[0].split("\\.");
		var callingDescriptor = "(" + tokDesc[1].strip();
		return new CallingMethod(Type.getObjectType(tokName[0]), tokName[1], Type.getMethodType(callingDescriptor), str);
	}
}
