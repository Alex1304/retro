package fr.umlv.retro.util;

import static org.objectweb.asm.Opcodes.V10;
import static org.objectweb.asm.Opcodes.V11;
import static org.objectweb.asm.Opcodes.V12;
import static org.objectweb.asm.Opcodes.V13;
import static org.objectweb.asm.Opcodes.V1_5;
import static org.objectweb.asm.Opcodes.V1_6;
import static org.objectweb.asm.Opcodes.V1_7;
import static org.objectweb.asm.Opcodes.V1_8;
import static org.objectweb.asm.Opcodes.V9;

import org.objectweb.asm.Opcodes;

public class JavaVersions {
	
	private JavaVersions() {
	}
	
	/**
	 * Parses a litteral Java version into an int matching {@link Opcodes}
	 * constants.
	 * 
	 * @param litteralVersion the version to parse
	 * @return the parsed java binary version number
	 * @throws NumberFormatException if the argument cannot be parsed to a number
	 */
	public static int parse(String litteralVersion) {
		switch (litteralVersion) {
			case "5": return V1_5;
			case "6": return V1_6;
			case "7": return V1_7;
			case "8": return V1_8;
			case "9": return V9;
			case "10": return V10;
			case "11": return V11;
			case "12": return V12;
			case "13": return V13;
			default: throw new NumberFormatException();
		}
	}

	/**
	 * Converts an int of a Java version to a human readable number as string.
	 * 
	 * @param version the int version to convert
	 * @return the version as string
	 * @throws IllegalArgumentException if the argument does not refer to a Java
	 *                                  version
	 */
	public static String toString(int version) {
		switch (version) {
			case V1_5: return "5";
			case V1_6: return "6";
			case V1_7: return "7";
			case V1_8: return "8";
			case V9: return "9";
			case V10: return "10";
			case V11: return "11";
			case V12: return "12";
			case V13: return "13";
			default: throw new IllegalArgumentException();
		}
	}
}
