package fr.umlv.retro.core;

import static java.util.Objects.requireNonNull;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import fr.umlv.retro.feature.Feature;

public class RetroFailedException extends RuntimeException {
	private static final long serialVersionUID = 6824076487089385499L;
	
	private final Map<String, Set<Feature>> unsupportedFeatures;
	
	public RetroFailedException(String message) {
		super(message);
		this.unsupportedFeatures = null;
	}
	
	public RetroFailedException(Map<String, Set<Feature>> unsupportedFeatures) {
		this.unsupportedFeatures = requireNonNull(unsupportedFeatures);
	}

	public Optional<Map<String, Set<Feature>>> getUnsupportedFeatures() {
		return Optional.ofNullable(unsupportedFeatures).map(Collections::unmodifiableMap);
	}
}
