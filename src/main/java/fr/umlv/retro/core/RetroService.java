package fr.umlv.retro.core;

import static java.util.stream.Collectors.toUnmodifiableSet;
import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import fr.umlv.retro.feature.Feature;
import fr.umlv.retro.feature.FeatureFactory;
import fr.umlv.retro.rest.json.requestparams.RetroParams;
import fr.umlv.retro.transform.ClassTransformer;
import fr.umlv.retro.transform.RetroClassVisitor;
import fr.umlv.retro.util.CommandLine;
import fr.umlv.retro.util.JavaVersions;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public class RetroService {
	
	public static void runFromCommandLine(CommandLine commandLine) throws IOException {
		run(
				commandLine.getArguments().stream(),
				commandLine.getOption("-features").stream().flatMap(v -> Arrays.stream(v.split(","))),
				commandLine.getOption("-target"),
				commandLine.getOption("--force").isPresent(),
				commandLine.getOption("-info").isPresent() ? System.out::println : null);
	}
	
	public static List<String> runInfoFromJson(RetroParams json) throws IOException {
		var infos = new ArrayList<String>();
		run(
				json.paths.stream(),
				json.features.stream(),
				Optional.empty(),
				false,
				infos::add);
		return infos;
	}
	
	public static void runTargetFromJson(RetroParams json) throws IOException {
		run(
				json.paths.stream(),
				json.features.stream(),
				Optional.ofNullable(json.target),
				json.force,
				null);
	}

	private static void run(Stream<String> pathStream, Stream<String> featureStream, Optional<String> targetOpt, boolean force, Consumer<String> infoConsumer) throws IOException {
		var featureFactories = extractFeatureFactories(featureStream);
		var paths = extractPaths(pathStream);
		var classTransformers = ClassTransformer.getAllClassTransformers();
		var target = infoConsumer == null ? extractTarget(targetOpt) : 0;
		var unsupportedFeatures = new LinkedHashMap<String, Set<Feature>>();
		var classTransformersToFlush = new LinkedHashSet<ClassTransformer>();
		for (var path : paths) {
			var classTransformer = findSupported(classTransformers, path);
			classTransformersToFlush.add(classTransformer);
			var synthClassRegistry = new SynthesizedClassRegistry();
			if (infoConsumer != null) {
				classTransformer.accept(path, classBytes -> {
					var cr = new ClassReader(classBytes);
					var rcv = new RetroClassVisitor(null, featureFactories, synthClassRegistry, unsupportedFeatures, 0, feature -> infoConsumer.accept(feature.getInfo()));
					cr.accept(rcv, 0);
				});
			} else {
				classTransformer.transform(path, synthClassRegistry, classBytes -> {
					var cr = new ClassReader(classBytes);
					var cw = new ClassWriter(COMPUTE_MAXS);
					var rcv = new RetroClassVisitor(cw, featureFactories, synthClassRegistry, unsupportedFeatures, target, feature -> {});
					cr.accept(rcv, 0);
					return cw.toByteArray();
				});
			}
		}
		if (!force && !unsupportedFeatures.isEmpty()) {
			throw new RetroFailedException(unsupportedFeatures);
		}
		for (var classTransformer : classTransformersToFlush) {
			classTransformer.flush();
		}
	}

	private static Set<FeatureFactory> extractFeatureFactories(Stream<String> featureStream) {
		var featureMap = Feature.getAllFeatures();
		var selectedFeatures = featureStream
				.map(featureName -> Optional.ofNullable(featureMap.get(featureName))
						.orElseThrow(() -> new InvalidParamsException("unknown feature: " + featureName)))
				.collect(toUnmodifiableSet());
		return selectedFeatures.isEmpty() ? Collections.unmodifiableSet(new HashSet<>(featureMap.values()))
				: selectedFeatures;
	}

	private static Set<Path> extractPaths(Stream<String> pathStream) {
		var paths = pathStream.map(Path::of).collect(toUnmodifiableSet());
		if (paths.isEmpty()) {
			throw new InvalidParamsException("no path specified");
		}
		return paths;
	}

	private static int extractTarget(Optional<String> targetOpt) {
		try {
			return targetOpt.map(JavaVersions::parse)
					.orElseThrow(() -> new InvalidParamsException("missing target"));
		} catch (NumberFormatException e) {
			throw new InvalidParamsException("invalid or unsupported version number for target");
		}
	}

	private static ClassTransformer findSupported(Set<ClassTransformer> classTransformers, Path path) {
		return classTransformers.stream().filter(tr -> tr.supports(path)).findAny()
				.orElseThrow(() -> new InvalidParamsException(path.toString() + " is an invalid or unsupported path"));
	}
}
