package fr.umlv.retro.core;

public class InvalidParamsException extends RuntimeException {
	private static final long serialVersionUID = -3145571691175989748L;

	public InvalidParamsException(String message) {
		super(message);
	}
}
