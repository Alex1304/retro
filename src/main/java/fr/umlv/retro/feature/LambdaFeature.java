package fr.umlv.retro.feature;

import static org.objectweb.asm.Opcodes.ACC_PRIVATE;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ASM7;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.V1_8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import fr.umlv.retro.transform.RewriteNotifier;
import fr.umlv.retro.util.JavaVersions;
import fr.umlv.retro.util.synthesize.CallingMethod;
import fr.umlv.retro.util.synthesize.SynthesizeUtils;
import fr.umlv.retro.util.synthesize.SynthesizedClass;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public class LambdaFeature extends AbstractFeature {
	
	private String thisClassName;
	private String functionalInterfaceName;
	private CallingMethod calling;
	private ArrayList<String> captured = new ArrayList<>();
	private int lambdaCount;
	private final Set<CallingMethod> wrappedRefs = new HashSet<>();
	

	public LambdaFeature(ClassWriter classWriter, int targetVersion, 
			SynthesizedClassRegistry synthClassRegistry, RewriteNotifier rewriteNotifier,
			Consumer<? super Feature> onRecongized, Map<String, Set<Feature>> unsupportedFeatures) {
		super(classWriter, targetVersion, synthClassRegistry, rewriteNotifier, onRecongized, unsupportedFeatures);
	}

	@Override
	public int releaseVersion() {
		return V1_8;
	}

	@Override
	public String getName() {
		return "LAMBDA";
	}
	
	@Override
	public String toString() {
		return "lambda " + functionalInterfaceName + " capture " + captured + " calling " + calling;
	}
	
	@Override
	public String getUnsupportedErrorMessage() {
		var v = JavaVersions.toString(releaseVersion());
		return "The code contains lambda expressions. If you downgrade to a target version lower than " + v
				+ ", please ensure that the two conditions below are met:\n"
				+ "- The code does not call any default methods from java.util.function interfaces (such as Function.andThen)\n"
				+ "- The code does not use any method references to methods introduced in Java " + v + " or later (such as Integer::sum)\n"
				+ "Both scenarios are likely to trigger java.lang.NoSuchMethodError at runtime.";
	}

	@Override
	public ClassVisitor getClassVisitor() {
		return new ClassVisitor(ASM7) {
			@Override
			public void visit(int version, int access, String name, String signature, String superName,
					String[] interfaces) {
				thisClassName = name;
			}
			
			@Override
			public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
				if (name.startsWith("lambda$")) {
					rewriteSubvisitor(cw -> cw.visitMethod(access - ACC_PRIVATE, name, descriptor, signature, exceptions), rewriteNotifier::setRewrittenMethodVisitor);
				}
				return new MethodVisitor(ASM7) {
					@Override
					public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments) {
						if (bootstrapMethodHandle.getOwner().equals("java/lang/invoke/LambdaMetafactory")
								&& bootstrapMethodHandle.getName().equals("metafactory")) {
							captureFeatureStartLineNumber();
							var capturedType = Type.getMethodType(descriptor);
							var fMethodType = Type.getMethodType(bootstrapMethodArguments[0].toString());
							var captureCount = capturedType.getArgumentTypes().length;
							var fMethodArgCount = fMethodType.getArgumentTypes().length;
							functionalInterfaceName = capturedType.getReturnType().getInternalName();
							calling = CallingMethod.parse(bootstrapMethodArguments[1].toString().replace(" .*", ""));
							
							if (calling.isMethodReference()) {
								wrapMethodRef(calling.isVirtualMethod(captureCount, fMethodArgCount));
							}
							for (var arg : capturedType.getArgumentTypes()) {
								captured.add(arg.getInternalName());
							}
							recognize();
							var synthClassName = thisClassName + "$lambda$" + (++lambdaCount);
							if (SynthesizeUtils.needsSynthesizedFunctionalInterface(functionalInterfaceName)) {
								functionalInterfaceName = SynthesizeUtils.adaptFunctionalInterfaceName(targetVersion, functionalInterfaceName);
								synthClassRegistry.add(SynthesizedClass.forFunctionalInterface(
										targetVersion,
										functionalInterfaceName,
										name,
										bootstrapMethodArguments[0].toString(),
										null));
							}
							var lambda = SynthesizedClass.forLambda(
									targetVersion,
									synthClassName,
									functionalInterfaceName,
									name,
									bootstrapMethodArguments[0].toString(),
									capturedType,
									calling);
							synthClassRegistry.add(lambda);
							rewriteMethodElement(mv -> mv.visitMethodInsn(INVOKESTATIC, synthClassName, "$init$", Type.getMethodDescriptor(
									Type.getObjectType(lambda.getInternalName()), capturedType.getArgumentTypes()), false));
							captured = new ArrayList<>();
						}
					}
				};
			}
		};
	}

	private void wrapMethodRef(boolean isVirtual) {
		var access = isVirtual ? 0 : ACC_STATIC;
		var name = "lambda$" + calling.getMethodName();
		var descriptor = calling.getMethodType().getDescriptor();
		if (wrappedRefs.add(calling)) {
			generateNewMethod(access, name, descriptor, g -> {
				if (isVirtual) {
					g.loadThis();
				}
				g.loadArgs();
				g.visitMethodInsn(isVirtual ? INVOKEVIRTUAL : INVOKESTATIC, calling.getOwner().getInternalName(), calling.getMethodName(), descriptor, false);
				g.returnValue();
			});
		}
		calling = calling.wrapped(Type.getObjectType(thisClassName), name);
	}
}
