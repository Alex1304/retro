package fr.umlv.retro.feature;

import static java.util.Objects.requireNonNullElse;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;

import fr.umlv.retro.transform.RewriteNotifier;
import fr.umlv.retro.util.MethodInfo;
import fr.umlv.retro.util.synthesize.SynthesizeUtils;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

abstract class AbstractFeature implements Feature {

	private final ClassWriter classWriter;
	final int targetVersion;
	final SynthesizedClassRegistry synthClassRegistry;
	final RewriteNotifier rewriteNotifier;
	private final Consumer<? super Feature> onRecognized;
	private final Map<String, Set<Feature>> unsupportedFeatures;
	
	private String className;
	private String source = "Unknown source";
	private MethodInfo methodInfo; // May be null
	private MethodVisitor methodWriter;
	private int currentLineNumber;
	private int featureStartLineNumber;

	public AbstractFeature(ClassWriter classWriter, int targetVersion,
			SynthesizedClassRegistry synthClassRegistry, RewriteNotifier rewriteNotifier,
			Consumer<? super Feature> onRecongized, Map<String, Set<Feature>> unsupportedFeatures) {
		this.classWriter = classWriter;
		this.targetVersion = targetVersion;
		this.synthClassRegistry = synthClassRegistry;
		this.rewriteNotifier = rewriteNotifier;
		this.onRecognized = onRecongized;
		this.unsupportedFeatures = unsupportedFeatures;
	}

	@Override
	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public void setSource(String source) {
		this.source = requireNonNullElse(source, "Unknown source");
	}

	@Override
	public void setMethodContext(MethodInfo info, MethodVisitor methodWriter) {
		this.methodInfo = info;
		this.methodWriter = methodWriter;
	}

	@Override
	public String getInfo() {
		return getName() + " at " + (methodInfo == null ? className : methodInfo) + " (" + source
				+ (featureStartLineNumber == 0 ? "" : ":" + featureStartLineNumber)
				+ "): " + toString();
	}

	@Override
	public void setLineNumber(int lineNumber) {
		this.currentLineNumber = lineNumber;
	}

	// ------------------------------------------------------
	// Package private methods to simplify code of subclasses
	// ------------------------------------------------------
	
	abstract int releaseVersion();

	void captureFeatureStartLineNumber() {
		this.featureStartLineNumber = currentLineNumber;
	}

	void rewriteClassElement(Consumer<ClassWriter> rewriteAction) {
		rewrite(classWriter, rewriteAction, true);
	}

	void rewriteMethodElement(Consumer<MethodVisitor> rewriteAction) {
		rewrite(methodWriter, rewriteAction, true);
	}

	<S> void rewriteSubvisitor(Function<ClassWriter, S> rewriteAction, Consumer<S> notifyAction) {
		rewrite(classWriter, cw -> notifyAction.accept(rewriteAction.apply(cw)), false);
	}
	
	void generateNewMethod(int access, String name, String descriptor, Consumer<GeneratorAdapter> generator) {
		if (classWriter != null) {
			SynthesizeUtils.synthesizeMethod(classWriter, access, new Method(name, descriptor), generator);
		}
	}

	void recognize() {
		onRecognized.accept(this);
	}
	
	private <W> void rewrite(W writer, Consumer<W> rewriteAction, boolean notifyRewrite) {
		if (writer != null && targetVersion < releaseVersion()) {
			unsupportedFeatures.computeIfAbsent(className + ".class", k -> new LinkedHashSet<>()).add(this);
			if (notifyRewrite) {
				rewriteNotifier.notifyRewrite();
			}
			rewriteAction.accept(writer);
		}
	}
}
