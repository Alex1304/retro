package fr.umlv.retro.feature;

import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.objectweb.asm.ClassWriter;

import fr.umlv.retro.transform.RewriteNotifier;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

/**
 * Factory to create features. They are instantiated as soon as enough info on
 * the class have been visited (such as class name and source name)
 */
@FunctionalInterface
public interface FeatureFactory {

	Feature create(ClassWriter classWriter, int targetVersion, 
			SynthesizedClassRegistry synthClassRegistry, RewriteNotifier rewriteNotifier,
			Consumer<? super Feature> onRecongized, Map<String, Set<Feature>> unsupportedFeatures);
}
