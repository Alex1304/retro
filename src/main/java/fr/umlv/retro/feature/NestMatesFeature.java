package fr.umlv.retro.feature;

import static org.objectweb.asm.Opcodes.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import fr.umlv.retro.transform.RewriteNotifier;
import fr.umlv.retro.util.JavaVersions;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

public class NestMatesFeature extends AbstractFeature {
	
	private String thisClassName;
	private String nestHost;
	private final ArrayList<String> nestMembers = new ArrayList<>();
	private final ArrayList<FieldInfo> nestMemberFields = new ArrayList<>();

	public NestMatesFeature(ClassWriter classWriter, int targetVersion, 
			SynthesizedClassRegistry synthClassRegistry, RewriteNotifier rewriteNotifier,
			Consumer<? super Feature> onRecongized, Map<String, Set<Feature>> unsupportedFeatures) {
		super(classWriter, targetVersion, synthClassRegistry, rewriteNotifier, onRecongized, unsupportedFeatures);
	}
	
	@Override
	public int releaseVersion() {
		return V11;
	}

	@Override
	public String getName() {
		return "NESTMATES";
	}
	
	@Override
	public String toString() {
		if (nestHost != null) {
			return "nestmate of " + nestHost;
		}
		return "nest host " + thisClassName + " members " + nestMembers;
	}
	
	@Override
	public String getUnsupportedErrorMessage() {
		return "Nestmates are a Java " + JavaVersions.toString(releaseVersion())
				+ " feature. Downgrading to a lower version will rewrite them in a such way that it "
				+ "will be fully compatible, but the rewritten bytecode might not be as optimized as "
				+ "the original.";
	}

	@Override
	public ClassVisitor getClassVisitor() {
		return new ClassVisitor(ASM7) {
			@Override
			public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
				thisClassName = name;
			}
			
			@Override
			public void visitNestHost(String nestHost) {
				NestMatesFeature.this.nestHost = nestHost;
				recognize();
				rewriteClassElement(cw -> {
					// Remove nest host in target class
				});
			}
			
			@Override
			public void visitNestMember(String nestMember) {
				nestMembers.add(nestMember);
				rewriteClassElement(cw -> {
					// Remove nest member in target class
				});
			}
			
			@Override
			public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
				if (nestHost != null) {
					nestMemberFields.add(new FieldInfo(name, descriptor));
				}
				return null;
			}
			
			@Override
			public void visitEnd() {
				if (!nestMemberFields.isEmpty()) {
					rewriteClassElement(cw -> {
						var methodAccess = ACC_SYNTHETIC | ACC_STATIC;
						for (var field : nestMemberFields) {
							// Generate getter
							var getterDescriptor = Type.getMethodDescriptor(
									Type.getType(field.descriptor),
									Type.getObjectType(thisClassName));
							var mv = cw.visitMethod(methodAccess, "get$" + field.name, getterDescriptor, null, null);
							mv.visitCode();
							mv.visitVarInsn(ALOAD, 0);
							mv.visitFieldInsn(GETFIELD, thisClassName, field.name, field.descriptor);
							mv.visitInsn(Type.getType(field.descriptor).getOpcode(IRETURN));
							mv.visitMaxs(1, 1);
							mv.visitEnd();
							// Generate setter
							var setterDescriptor = Type.getMethodDescriptor(
									Type.getType(void.class),
									Type.getObjectType(thisClassName),
									Type.getType(field.descriptor));
							mv = cw.visitMethod(methodAccess, "set$" + field.name, setterDescriptor, null, null);
							mv.visitCode();
							mv.visitVarInsn(ALOAD, 0);
							mv.visitVarInsn(Type.getType(field.descriptor).getOpcode(ILOAD), 1);
							mv.visitFieldInsn(PUTFIELD, thisClassName, field.name, field.descriptor);
							mv.visitInsn(RETURN);
							mv.visitMaxs(2, 1);
							mv.visitEnd();
						}
						cw.visitEnd();
					});
				}
				if (!nestMembers.isEmpty()) {
					recognize();
				}
			}
			
			@Override
			public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
				if (nestHost != null) {
					rewriteSubvisitor(
							cw -> cw.visitMethod(access - (access & ACC_PRIVATE), name, descriptor, signature, exceptions),
							rewriteNotifier::setRewrittenMethodVisitor);
				}
				return new MethodVisitor(ASM7) {
					@Override
					public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
						if ((opcode == GETFIELD || opcode == PUTFIELD) && nestMembers.contains(owner)) {
							rewriteMethodElement(mv -> {
								var invokeName = (opcode == GETFIELD ? "get" : "set") + '$' + name;
								String invokeDescriptor;
								if (opcode == GETFIELD) {
									invokeDescriptor = Type.getMethodDescriptor(
											Type.getType(descriptor),
											Type.getObjectType(owner));
								} else {
									invokeDescriptor = Type.getMethodDescriptor(
											Type.getType(void.class),
											Type.getObjectType(owner),
											Type.getType(descriptor));
								}
								mv.visitMethodInsn(INVOKESTATIC, owner, invokeName, invokeDescriptor, false);
							});
						}
					}
				};
			}
		};
	}

	private static class FieldInfo {
		private final String name, descriptor;
		public FieldInfo(String name, String descriptor) {
			this.name = name;
			this.descriptor = descriptor;
		}
	}
}
