package fr.umlv.retro.feature;

import static org.objectweb.asm.Opcodes.ASM7;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.DUP_X1;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.NEW;
import static org.objectweb.asm.Opcodes.SWAP;
import static org.objectweb.asm.Opcodes.V14;

import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import fr.umlv.retro.transform.RewriteNotifier;
import fr.umlv.retro.util.JavaVersions;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

/**
 * Represents the Java 14 Record feature. It currently does not support
 * rewriting equals and hashCode when downgrading.
 */
public final class RecordFeature extends AbstractFeature {

	private boolean extendsJavaLangRecord;
	private String fields;

	public RecordFeature(ClassWriter classWriter, int targetVersion, 
			SynthesizedClassRegistry synthClassRegistry, RewriteNotifier rewriteNotifier,
			Consumer<? super Feature> onRecongized, Map<String, Set<Feature>> unsupportedFeatures) {
		super(classWriter, targetVersion, synthClassRegistry, rewriteNotifier, onRecongized, unsupportedFeatures);
	}

	@Override
	public String getName() {
		return "RECORD";
	}

	@Override
	public int releaseVersion() {
		return V14;
	}
	
	@Override
	public String toString() {
		return "record with fields " + fields.replace(";", ", ");
	}
	
	@Override
	public String getUnsupportedErrorMessage() {
		return "Records are a new feature introduced in Java " + JavaVersions.toString(releaseVersion()) + ". "
				+ "The downgrade will rewrite a record as a regular class containing the same fields. "
				+ "They should work fine in most cases, however you might encounter issues with equals and hashCode. "
				+ "Those methods are normally overriden in records but these overrides will be removed in the target code, "
				+ "as they are difficult to rewrite. Only toString() will be rewritten and function as normal. "
				+ "It is not recommended to downgrade records if your code is reliant on equals and hashCode behavior.";
	}

	@Override
	public ClassVisitor getClassVisitor() {
		return new ClassVisitor(ASM7) {
			@Override
			public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
				if ("java/lang/Record".equals(superName)) {
					extendsJavaLangRecord = true;
					rewriteClassElement(cw -> cw.visit(version, access, name, signature, Type.getInternalName(Object.class), interfaces));
				}
			}
			
			@Override
			public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
				if (extendsJavaLangRecord && (name.equals("equals") || name.equals("hashCode"))) {
					rewriteNotifier.requestCodeRemoval(); // Remove equals and hashCode methods completely
					return null;
				}
				return new MethodVisitor(ASM7) {
					@Override
					public void visitMethodInsn(int opcode, String owner, String name, String descriptor,
							boolean isInterface) {
						if (extendsJavaLangRecord && opcode == INVOKESPECIAL && name.equals("<init>")) {
							rewriteMethodElement(mv -> mv.visitMethodInsn(opcode, "java/lang/Object", name, descriptor, isInterface));
						}
					}
					
					@Override
					public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments) {
						if (extendsJavaLangRecord && name.equals("toString")) {
							fields = bootstrapMethodArguments[1].toString();
							rewriteMethodElement(mv -> { // Rewriting toString using a StringBuilder appending each field
								var ownerDesc = Type.getType(bootstrapMethodArguments[0].toString());
								var owner = ownerDesc.getInternalName();
								mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
								mv.visitInsn(DUP);
								mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
								mv.visitLdcInsn(owner.replace('/', '.').replace('$', '.') + "[");
								mv.visitMethodInsn(INVOKEVIRTUAL,  "java/lang/StringBuilder", "append", Type.getMethodDescriptor(
										Type.getType(StringBuilder.class), Type.getType(String.class)), false);
								var fieldNames = bootstrapMethodArguments[1].toString().split(";");
								var i = 0;
								for (var fieldName : fieldNames) {
									mv.visitLdcInsn(fieldName + "=");
									mv.visitMethodInsn(INVOKEVIRTUAL,  "java/lang/StringBuilder", "append", Type.getMethodDescriptor(
											Type.getType(StringBuilder.class), Type.getType(String.class)), false);
									var fieldDescriptor = bootstrapMethodArguments[i + 2].toString().replaceAll(" \\([0-9]*\\)", "")
											.replace(owner + "." + fieldName, "");
									mv.visitInsn(SWAP);
									mv.visitInsn(DUP_X1);
									mv.visitFieldInsn(GETFIELD, owner, fieldName, fieldDescriptor);
									if (fieldDescriptor.startsWith("L") && !fieldDescriptor.equals(Type.getDescriptor(String.class))) {
										fieldDescriptor = Type.getDescriptor(Object.class);
									}
									mv.visitMethodInsn(INVOKEVIRTUAL,  "java/lang/StringBuilder", "append", Type.getMethodDescriptor(
											Type.getType(StringBuilder.class), Type.getType(fieldDescriptor)), false);
									if (i < fieldNames.length - 1) {
										mv.visitLdcInsn(", ");
										mv.visitMethodInsn(INVOKEVIRTUAL,  "java/lang/StringBuilder", "append", Type.getMethodDescriptor(
												Type.getType(StringBuilder.class), Type.getType(String.class)), false);
									}
									i++;
								}
								mv.visitLdcInsn("]");
								mv.visitMethodInsn(INVOKEVIRTUAL,  "java/lang/StringBuilder", "append", Type.getMethodDescriptor(
										Type.getType(StringBuilder.class), Type.getType(String.class)), false);
								mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", Type.getMethodDescriptor(Type.getType(String.class)), false);
							});
						}
					}
				};
			}
			
			@Override
			public void visitEnd() {
				if (extendsJavaLangRecord) {
					recognize();
				}
			}
		};
	}
}
