package fr.umlv.retro.feature;

import java.util.Map;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import fr.umlv.retro.util.MethodInfo;

/**
 * A {@link Feature} is defined by one or several instructions to be recognized,
 * in order to display them or retro them. A {@link Feature} instance belongs to
 * one class, so it is not possible to use the same {@link Feature} instance for
 * several classes. Use the {@link FeatureFactory} as an utility to instantiate
 * a {@link Feature} for each class.
 */
public interface Feature {
	/**
	 * Sets the name of the class that is being visited.
	 * 
	 * @param clasName the class name
	 */
	void setClassName(String className);
	
	/**
	 * Sets the source of the class that is being visited.
	 * 
	 * @param source the source
	 */
	void setSource(String source);

	/**
	 * Gets the name of the feature.
	 * 
	 * @return a String
	 */
	String getName();
	
	/**
	 * Gets the message to display when this feature is recognized but unsupported
	 * for the specified target version.
	 * 
	 * @return a String
	 */
	String getUnsupportedErrorMessage();

	/**
	 * Sets the context of a method that is being visited.
	 * 
	 * @param info    information on the method (name, descriptor, etc)
	 * @param methodWriter    the method visitor that can be used to rewrite instructions
	 */
	void setMethodContext(MethodInfo info, MethodVisitor methodWriter);

	/**
	 * Sets the line number.
	 * 
	 * @param line the line number
	 */
	void setLineNumber(int line);

	/**
	 * get information on this feature.
	 */
	String getInfo();

	/**
	 * Gets the class visitor implemented by this feature.
	 * 
	 * @return a class visitor
	 */
	ClassVisitor getClassVisitor();
	
	/**
	 * Gets all features useful for the application.
	 * 
	 * @return a Map keyed by feature names and with the factories as value
	 */
	static Map<String, FeatureFactory> getAllFeatures() {
		return Map.<String, FeatureFactory>ofEntries(
				Map.entry("concatenation", StringConcatenationFeature::new),
				Map.entry("try_with_resources", TryWithResourcesFeature::new),
				Map.entry("lambda", LambdaFeature::new),
				Map.entry("nestmates", NestMatesFeature::new),
				Map.entry("record", RecordFeature::new)
		);
	}

}
