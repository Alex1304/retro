package fr.umlv.retro.feature;

import static fr.umlv.retro.util.synthesize.SynthesizeUtils.isPrimitive;
import static org.objectweb.asm.Opcodes.ACC_PRIVATE;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ASM7;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.V9;

import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.Method;

import fr.umlv.retro.transform.RewriteNotifier;
import fr.umlv.retro.util.JavaVersions;
import fr.umlv.retro.util.synthesize.SynthesizedClassRegistry;

/**
 * Feature that recognizes String Concatenation in Java 9 and higher. Offers the
 * ability to downgrade it by replacing the invokedynamic instruction by the use
 * of a StringBuilder.
 */
public final class StringConcatenationFeature extends AbstractFeature {

	private String thisClassName;
	private String concatPattern;
	private int concatId;

	public StringConcatenationFeature(ClassWriter classWriter, int targetVersion, 
			SynthesizedClassRegistry synthClassRegistry, RewriteNotifier rewriteNotifier,
			Consumer<? super Feature> onRecongized, Map<String, Set<Feature>> unsupportedFeatures) {
		super(classWriter, targetVersion, synthClassRegistry, rewriteNotifier, onRecongized, unsupportedFeatures);
	}

	@Override
	public int releaseVersion() {
		return V9;
	}
	
	@Override
	public String getName() {
		return "CONCATENATION";
	}
	
	@Override
	public String toString() {
		return "pattern " + concatPattern.replace("\u0001", "%1");
	}
	
	@Override
	public String getUnsupportedErrorMessage() {
		return "The implementation of String concatenation in bytecode has been updated in Java "
				+ JavaVersions.toString(releaseVersion())
				+ " with invokedynamic calls. Downgrading to a lower version will replace those invokedynamic calls with a "
				+ "regular StringBuilder so that it will be fully compatible with older VMs, but the rewritten bytecode "
				+ "might not be as optimized as the original.";
	}

	@Override
	public ClassVisitor getClassVisitor() {
		return new ClassVisitor(ASM7) {
			
			@Override
			public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
				thisClassName = name;
			}
			
			@Override
			public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
					String[] exceptions) {
				return new MethodVisitor(ASM7) {
					@Override
					public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments) {
						if (name.equals("makeConcatWithConstants")
								&& bootstrapMethodHandle.getOwner().equals("java/lang/invoke/StringConcatFactory")
								&& bootstrapMethodHandle.getName().equals("makeConcatWithConstants")
								&& bootstrapMethodArguments.length == 1) {
							captureFeatureStartLineNumber();
							concatPattern = bootstrapMethodArguments[0].toString();
							recognize();
							var stringBuilderType = Type.getType(StringBuilder.class);
							var tokens = concatPattern.split("\u0001", -1);
							var id = ++concatId;
							generateNewMethod(ACC_PRIVATE | ACC_STATIC, concatMethodName(id), descriptor, g -> {
								g.newInstance(stringBuilderType);
								g.dup();
								g.invokeConstructor(stringBuilderType, Method.getMethod("void <init> ()"));
								var argIndex = 0;
								for (var i = 0 ; i < tokens.length ; i++) {
									if (!tokens[i].isEmpty()) {
										g.push(tokens[i]);
										var appendDesc = Type.getMethodDescriptor(stringBuilderType, Type.getType(String.class));
										g.invokeVirtual(stringBuilderType, new Method("append", appendDesc));
									}
									if (argIndex < g.getArgumentTypes().length) {
										g.loadArg(argIndex++);
										var appendArgType = g.getArgumentTypes()[i];
										if (!isPrimitive(appendArgType) && !appendArgType.getDescriptor().equals(Type.getDescriptor(String.class))) {
											appendArgType = Type.getType(Object.class);
										}
										g.invokeVirtual(stringBuilderType, new Method("append", Type.getMethodDescriptor(stringBuilderType, appendArgType)));
									}
								}
								g.invokeVirtual(stringBuilderType, Method.getMethod("String toString ()"));
								g.returnValue();
							});
							rewriteMethodElement(mv -> mv.visitMethodInsn(INVOKESTATIC, thisClassName, concatMethodName(id), descriptor, false));
							concatPattern = null;
						}
					}
				};
			}
		};
	}
	
	private static String concatMethodName(int id) {
		return "concat$" + id;
	}
}
