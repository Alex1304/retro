package fr.umlv.retro;

import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import fr.umlv.retro.core.InvalidParamsException;
import fr.umlv.retro.core.RetroFailedException;
import fr.umlv.retro.core.RetroService;
import fr.umlv.retro.util.CommandLine;

public class Main {

	public static void main(String[] args) {
		try {
			var commandLine = CommandLine.parse(args, Map.ofEntries(
					Map.entry("-help", false),
					Map.entry("-features", true),
					Map.entry("-target", true),
					Map.entry("-info", false),
					Map.entry("--force", false)
			));
			if (commandLine.getOption("-help").isPresent()) {
				printHelp();
				return;
			}
			RetroService.runFromCommandLine(commandLine);
		} catch (InvalidParamsException e) {
			System.err.println("Command syntax error: " + e.getMessage());
			System.err.println("\nUse -help for more information.");
			System.exit(1);
			return;
		} catch (RetroFailedException e) {
			e.getUnsupportedFeatures().ifPresentOrElse(unsupportedFeatures -> {
				var errorMsg = new StringBuilder("The target version does not support some of the features "
						+ "found in your input files:\n");
				unsupportedFeatures.forEach((className, features) -> {
					errorMsg.append("In ").append(className).append(":\n");
					errorMsg.append(features.stream()
							.map(feature -> "\t- " + feature.getName() + '\n')
							.collect(joining()));
				});
				errorMsg.append("\nDetails on the features:\n");
				errorMsg.append(unsupportedFeatures.values().stream()
						.flatMap(Set::stream)
						.map(feature -> "=> " + feature.getName() + ": " + feature.getUnsupportedErrorMessage() + '\n')
						.distinct()
						.collect(joining()));
				errorMsg.append("\nRun again with --force if you are sure you want to downgrade anyway.");
				System.err.println(errorMsg.toString());
			}, () -> System.err.println(e.getMessage()));
			System.exit(1);
			return;
		} catch (IOException e) {
			if (e.getMessage() == null) {
				System.err.println("An unknown I/O error occured when processing the files.");
			} else {
				System.err.println("An I/O error occured when processing the files: " + e.getMessage());
			}
			System.exit(1);
			return;
		}
	}

	private static void printHelp() {
		System.out.println("Usage: retro [options] <file1> [<file2> ...]");
		System.out.println("-help\n\tPrints this help message.");
		System.out.println("-features <feature1>[,<feature2>,...]\n\tFilters the list of features to detect."
				+ "Available features: concatenation, try_with_resources, lambda, nestmates, record.");
		System.out.println("-target <version>\n\tThe target Java version to retro to.");
		System.out.println("-info\n\tJust prints the features present in the class without performing the retro.");
		System.out.println("--force\n\tDowngrade class files even if they contain features unsupported by the target version (attempts to rewrite the features the best it can)");
	}
}
