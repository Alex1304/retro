module retro {
	requires transitive org.objectweb.asm;
	requires transitive org.objectweb.asm.commons;
	requires transitive java.ws.rs;
	requires jakarta.enterprise.cdi.api;
	requires resteasy.core.spi;
	requires org.jboss.logging;
	requires java.validation;
	requires smallrye.config;

	exports fr.umlv.retro.core;
	exports fr.umlv.retro.feature;
	exports fr.umlv.retro.rest.json.error;
	exports fr.umlv.retro.rest.json.requestparams;
	exports fr.umlv.retro.rest.json.response;
	exports fr.umlv.retro.transform;
	exports fr.umlv.retro.util;
	exports fr.umlv.retro.util.synthesize;

	opens fr.umlv.retro.rest.resource;
	opens fr.umlv.retro.rest.json.error;
	opens fr.umlv.retro.rest.json.requestparams;
	opens fr.umlv.retro.rest.json.response;
}