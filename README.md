**Retro**

Project by Alexandre MIRANDA and Axel ZEMMOUR, tutored by Remi FORAX.

**Compilation**

Compile the projet using Maven:
```
cd retro
mvn package
```

**Run the program in command line**

After compiling successfully, use the following command to run the program, assuming your working directory is the root of the project:
```
java -jar target/retro.jar <PROGRAM ARGS AND OPTIONS>
```
Use the `-help` option to learn about the usage.

**Run the Quarkus server**

You can run the Quarkus server with the line:
```
java -jar target/retro-server-runner.jar
```

**Run the font-end**

Go to the front directory:
```
cd front
```
Then install the node dependencies:
```
npm install
```
Launch it using the command:
```
npm start
```

Go to http://localhost:3000 to access the front app.

**Missing features**

The project is almost complete, the following features are missing:
- Rewrite of `hashCode` and `equals` when force-downgrading a Record
- Default methods for generated functional interfaces
- Method references to methods not existing in older JDKs (e.g Integer::sum)

**Known bugs**

- Downgrading to Java < 8 code that contains static methods in interfaces (e.g List.of, Map.entry, etc), executing it will crash the JVM