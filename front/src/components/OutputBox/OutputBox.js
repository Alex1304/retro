import React from 'react';
import './OutputBox.css';

import { connect } from 'react-redux';

const OutputBox = ({ output }) => (<>
    <h3>Output:</h3>
    <div id="output" className="OutputBox">
        {output}
    </div>
</>);

function mapStateToProps(state) {
    return {
        output: state.output_box
    };
}

export default connect(mapStateToProps)(OutputBox);
