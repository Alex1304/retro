import React from 'react';
import './FileList.css';

import { connect } from 'react-redux';

import Button from '../Button';

import { ReactComponent as File } from './document.svg';
import { ReactComponent as Folder } from './folder.svg';

import * as actions from '../../actions.js';

function foldersFirstThenAlphabetical(a, b) {
    let dir = (a.isDirectory ? 0 : 1) - (b.isDirectory ? 0 : 1);
    if (!dir) {
        return a.path.localeCompare(b.path);
    }
    return dir;
}

function fileName(separator, file) {
    let tokens = file.path.split(separator);
    return tokens[tokens.length - 1];
}

function fileCanBeAdded(to_process, file) {
    return !to_process.includes(file.path) &&
            (file.isDirectory || file.path.endsWith('.class') || file.path.endsWith('.jar'));
}

const FileList = ({ tree, to_process, dispatch }) => (
    <div className="table-responsive">
        <table className="FileList table table-hover table-dark">
            <thead>
                <tr>
                    <td>
                        <span className="current-path">{tree.directory}</span>
                    </td>
                    <td>
                        <Button text="Go to parent" onClick={() => {
                            let parent = tree.directory + tree.separator + '..';
                            dispatch(actions.updateDirectory(parent));
                            dispatch(actions.asyncUpdateTree(parent));
                        }} />
                    </td>
                </tr>
            </thead>
            <tbody>
                {tree.entries.sort(foldersFirstThenAlphabetical).map((file, i) => (
                    <tr key={i}>
                        <td className={file.isDirectory ? "clickable" : ""} onClick={() => {
                            if (file.isDirectory) {
                                dispatch(actions.updateDirectory(file.path));
                                dispatch(actions.asyncUpdateTree(file.path));
                            }
                        }}>
                            {(file.isDirectory && <Folder width="20" />) || <File width="20" />} {fileName(tree.separator, file)}
                        </td>
                        <td>
                            {fileCanBeAdded(to_process, file) && <Button type="primary btn-sm" text="Add" onClick={() => dispatch(actions.toProcessAdd(file.path))} />}
                            {to_process.includes(file.path) && <span className="added">Added</span>}
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    </div>
);

function mapStateToProps(state) {
    return {
        tree: state.tree,
        to_process: state.to_process
    };
}

export default connect(mapStateToProps)(FileList);
