import React from 'react';
import './AddedFiles.css';

import { connect } from 'react-redux';

import Button from '../Button';

import * as actions from '../../actions.js';

const AddedFiles = ({ to_process, dispatch }) => (
    <div className="table-responsive">
        <table className="AddedFiles table table-dark">
            <tbody>
                {to_process.map((file, i) => (
                    <tr key={i}>
                        <td>
                            {file}
                        </td>
                        <td>
                            <Button type="danger btn-sm" text="Remove" onClick={() => dispatch(actions.toProcessRemove(file))} />
                        </td>
                    </tr>
                ))}
                {to_process.length === 0 &&
                    <tr>
                        <td>
                            No items added yet.
                        </td>
                    </tr>
                }
            </tbody>
        </table>
    </div>
);

function mapStateToProps(state) {
    return {
        tree: state.tree,
        to_process: state.to_process
    };
}

export default connect(mapStateToProps)(AddedFiles);
