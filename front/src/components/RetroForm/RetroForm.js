import React from 'react';
import './RetroForm.css';

import { connect } from 'react-redux';

import Button from '../Button';
import * as actions from '../../actions.js';

function optionsCollectionToArray(options) {
    let result = [];
    for (let i = 0 ; i < options.length ; i++) {
        result.push(options[i]);
    }
    return result;
}

const RetroForm = ({ dispatch, to_process }) => (
    <form id="retro-form" className="RetroForm" onSubmit={event => {
        event.preventDefault();
        let target = document.getElementById('targetVersion').value;
        let features = optionsCollectionToArray(document.getElementById('features').options)
                .filter(option => option.selected)
                .map(option => option.value);
        let runOption = document.getElementById('retro-form').elements['runOptions'].value;
        let force = document.getElementById('force').checked;
        dispatch(actions.asyncRetro({
            paths: to_process,
            target,
            features,
            force
        }, runOption === '1'));
    }}>
        <div className="form-group">
            <label htmlFor="targetVersion">Target version</label>
            <select className="form-control" id="targetVersion" defaultValue="8">
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
                <option>13</option>
            </select>
        </div>
        <div className="form-group">
            <label htmlFor="features">Features</label>
            <select multiple className="form-control" id="features">
                <option>concatenation</option>
                <option>try_with_resources</option>
                <option>lambda</option>
                <option>nestmates</option>
                <option>record</option>
            </select>
        </div>
        <div className="form-group">
            <div className="form-check">
                <input className="form-check-input" type="radio" name="runOptions" value="1" id="infoOption" defaultChecked
                    onChange={() => document.getElementById('force').disabled = true} />
                <label htmlFor="infoOption" className="form-check-label">Output the list of features detected in the selected items</label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="radio" name="runOptions" value="0" id="executeOption"
                    onChange={() => document.getElementById('force').disabled = false} />
                <label htmlFor="executeOption" className="form-check-label">Perform the downgrade of the input items to the selected target version</label>
            </div>
            <div className="form-check RetroForm-indent">
                <input className="form-check-input" type="checkbox" id="force" disabled />
                <label htmlFor="force" className="form-check-label">Force if the input files contain features that are unsupported in the target version</label>
            </div>
        </div>
        <Button text="Go!" isSubmit />
    </form>
);

function mapStateToProps(state) {
    return {
        to_process: state.to_process
    };
}

export default connect(mapStateToProps)(RetroForm);
