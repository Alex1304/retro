import { combineReducers } from 'redux';
import * as actions from './actions.js';

function tree(state = { directory: '.', entries: [] }, action) {
    switch (action.type) {
        case actions.UPDATE_TREE:
            return action.data;
        case actions.UPDATE_DIRECTORY:
            state.directory = action.directory;
            return state;
        default:
            return state;
    }
}

function overlay(state = {}, action) {
    switch (action.type) {
        case actions.SUBMIT:
            return { icon: 'LOADING' };
        case actions.SUCCESS:
            return {
                icon: 'SUCCESS',
                text: action.successMessage,
                button: action.buttonText,
                onClick: action.onClick,
            };
        case actions.ERROR:
            return {
                icon: 'FAILED',
                text: action.errorMessage,
                button: action.buttonText,
                onClick: action.onClick,
            };
        case actions.OVERLAY_DISMISS:
            return {};
        default:
            return state;
    }
}

function to_process(state = [], action) {
    switch (action.type) {
        case actions.TO_PROCESS_ADD: {
            let newState = state.slice();
            newState.push(action.path);
            return newState;
        }
        case actions.TO_PROCESS_REMOVE: {
            let newState = state.slice();
            newState.splice(newState.indexOf(action.path));
            return newState;
        }
        default:
            return state;
    }
}

function output_box(state = '', action) {
    switch (action.type) {
        case actions.UPDATE_OUTPUT_BOX:
            return action.content;
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    tree,
    overlay,
    to_process,
    output_box
});

export default rootReducer;
