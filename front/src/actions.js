import fetch from 'cross-fetch';

import config from './config.json';

export const UPDATE_TREE = 'UPDATE_TREE';
export const UPDATE_DIRECTORY = 'UPDATE_DIRECTORY';
export const SUBMIT = 'SUBMIT';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';
export const OVERLAY_DISMISS = 'OVERLAY_DISMISS';
export const TO_PROCESS_ADD = 'TO_PROCESS_ADD';
export const TO_PROCESS_REMOVE = 'TO_PROCESS_REMOVE';
export const UPDATE_OUTPUT_BOX = 'UPDATE_OUTPUT_BOX';

export function submit() {
    return {
        type: SUBMIT,
    };
}

export function receiveError(errorMessage, buttonText = 'OK', onClick = null) {
    return {
        type: ERROR,
        errorMessage,
        buttonText,
        onClick,
    };
}

export function receiveSuccess(successMessage, buttonText = 'OK', onClick = null) {
    return {
        type: SUCCESS,
        successMessage,
        buttonText,
        onClick,
    };
}

export function dismissOverlay() {
    return {
        type: OVERLAY_DISMISS,
    };
}

export function updateTree(data) {
    return {
        type: UPDATE_TREE,
        data
    };
}

export function updateDirectory(directory) {
    return {
        type: UPDATE_DIRECTORY,
        directory
    };
}

export function toProcessAdd(path) {
    return {
        type: TO_PROCESS_ADD,
        path
    };
}

export function toProcessRemove(path) {
    return {
        type: TO_PROCESS_REMOVE,
        path
    };
}

export function updateOutputBox(content) {
    return {
        type: UPDATE_OUTPUT_BOX,
        content
    };
}

// ASYNC ACTIONS

const headers = {
    'Content-Type': 'application/json'
};

const processResponse = response => {
    return response.text().then(raw => {
        if (!raw) {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return null;
        }
        let data = JSON.parse(raw);
        if (!response.ok) {
            let error = new Error(response.statusText);
            error.data = data;
            throw error;
        }
        return data;
    });
};

export function asyncUpdateTree(directory) {
    return (dispatch, getState) => {
        dispatch(submit());

        return fetch(config.api_url + "/tree", {
            method: 'POST',
            headers,
            body: JSON.stringify({ directory })
        })
        .then(processResponse)
        .then(data => {
            dispatch(dismissOverlay());
            dispatch(updateTree(data));
        })
        .catch(error => dispatch(receiveError(error.data ? error.data.message : error.message)));
    };
}

export function asyncRetro(retroParams, info) {
    return (dispatch, getState) => {
        dispatch(submit());

        return fetch(config.api_url + "/retro/" + (info ? "info" : "execute") , {
            method: 'POST',
            headers,
            body: JSON.stringify(retroParams)
        })
        .then(processResponse)
        .then(data => {
            if (!data) {
                dispatch(receiveSuccess('Retro completed successfully!', 'Close'));
                dispatch(updateOutputBox('Retro completed successfully!'));
            } else {
                dispatch(dismissOverlay());
                dispatch(updateOutputBox(data.join('\n') + '\n'));
            }
        })
        .catch(error => {
            if (error.data) {
                if (error.data.message) {
                    dispatch(receiveError(error.data.message));
                }
                if (error.data.violations) {
                    dispatch(updateOutputBox(error.data.violations.join('\n') + '\n'));
                }
                if (error.data.aboutFeatures && error.data.unsupportedFeatures) {
                    dispatch(receiveError('Your input files contained features that are unsupported in the target version. Run again with the force option.'));
                    let output = error.data.unsupportedFeatures.map(uf =>
                                    'In ' + uf.classFile + ':\n'
                                    + uf.features.map(f => '\t- ' + f + '\n').join('')).join('')
                            + 'Details about features:\n'
                            + error.data.aboutFeatures.map(f => f.name + ': ' + f.explanation + '\n').join('')
                            + 'If you are sure you want to execute the retro, check the "force" box and try again.\n';
                    dispatch(updateOutputBox(output));
                }
            } else {
                dispatch(receiveError(error.message));
            }
        });
    };
}
