import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Home.css';

import * as actions from '../../actions.js';
import FileList from '../../components/FileList';
import AddedFiles from '../../components/AddedFiles';
import RetroForm from '../../components/RetroForm';
import OutputBox from '../../components/OutputBox';

class Home extends Component {

    componentDidMount() {
        this.props.dispatch(actions.asyncUpdateTree(this.props.tree.directory));
    }

    render() {
        return (
            <div className="Home">
                <h2>Select the files to process</h2>
                <p>Click "Add" next to the desired files or folders</p>
                <FileList />
                <h2>Review the items you've added</h2>
                <p>Click "Remove" next to items you want to remove</p>
                <AddedFiles />
                {this.props.to_process.length > 0 && <>
                    <h2>Ready to retro?</h2>
                    <RetroForm />
                    <OutputBox />
                </>}

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        tree: state.tree,
        to_process: state.to_process
    };
}

export default connect(mapStateToProps)(Home);
