import React, { Component } from 'react';
import './App.css';

import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './../../configureStore';

import Home from '../Home';
import Header from '../../components/Header';
import Overlay from '../../components/Overlay';

const store = configureStore({});

class App extends Component {

    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div className="App">
                        <Header title="Retro" />
                        <hr />
                        <Overlay />
                        <Switch>
                            <Route exact path="/" component={Home} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
